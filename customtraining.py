import os
import sys
import gzip
import pickle
import numpy as np
import vidtrain

data_path = '/home/diezlab/training_data/'
with gzip.open(os.path.join(data_path, 'training_data.pkl.gz'), 'rb') as f:
    data = pickle.load(f)
with gzip.open(os.path.join(data_path, 'annotation_data_frame_dl_predicted_0_200.pkl.gz'), 'rb') as f:
    annotation = pickle.load(f)
first_annotated = 0
last_annotated = 201
for ev_type in data.keys():
    data[ev_type] = data[ev_type][first_annotated:last_annotated]
    annotation[ev_type] = annotation[ev_type][first_annotated:last_annotated]
x = []
y = []
for ev_type in data.keys():
    x += data[ev_type]
    y += annotation[ev_type]
with gzip.open(os.path.join(data_path, 'training_data_full_stack_junctions_193_277_177_187.pkl.gz'), 'rb') as f:
    data_full = pickle.load(f)
with gzip.open(os.path.join(data_path, 'verified_training_labels_full_stack_junctions_193_277_177_187.pkl.gz'), 'rb') as f:
    annotation_full = pickle.load(f)
x += list(np.squeeze(data_full))
y += list(annotation_full)
for i in range(len(x)):
    x[i] = x[i][..., np.newaxis]
ms = vidtrain.datahandler.MultiImageStack()
ms.add_list(x)
ann = vidtrain.datahandler.MultiImageStackClassification()
ann.add_list(y)
wf = vidtrain.datahandler.JunctionAnalysisData(processed=ms, annotation=ann)
root = vidtrain.gui.RootWindow()
trainer = vidtrain.workflow.training.JunctionNetworkTraining(root, wf)
trainer.show()
root.run()
