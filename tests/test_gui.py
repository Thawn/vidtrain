import micdata
import unittest
import sys
import os
import shutil
import glob
import tkinter
import tempfile
import numpy as np
import tifffile
try:
    import vidtrain
except ModuleNotFoundError:
    sys.path.append(os.path.dirname(os.path.dirname(__file__)))
    import vidtrain

testdir = os.path.dirname(__file__)


class GUIBase(unittest.TestCase):
    def setUp(self):
        self.root = vidtrain.gui.backend.RootWindow()
        self.stack = vidtrain.datahandler.ImageStack()
        self.multi_stack = vidtrain.datahandler.MultiImageStack()
        self.workflow_data = vidtrain.datahandler.JunctionAnalysisData()

    def tearDown(self):
        try:
            self.root.destroy()
        except:
            pass


class GUIWithData(GUIBase):
    def setUp(self):
        super().setUp()
        self.tempdir = tempfile.mkdtemp()
        self.eval_path = os.path.join(self.tempdir, 'teststack_eval')
        self.stack_path = os.path.join(self.tempdir, 'teststack.tif')
        try:
            shutil.copytree(os.path.join(testdir, 'teststack_eval'), self.eval_path)
            shutil.copyfile(os.path.join(testdir, 'teststack.tif'), self.stack_path)
        except FileExistsError:
            pass
        self.workflow_data.path = self.stack_path

    def tearDown(self):
        super().tearDown()
        shutil.rmtree(self.tempdir, ignore_errors=True)


class TestNetworkList(GUIBase):
    def test_interactive(self):
        network_list = vidtrain.datahandler.NetworkList()
        np = vidtrain.gui.backend.NetworkListPanel(master=self.root, network_list=network_list)
        np.show()
        self.root.run()


class TestConfigPanel(GUIBase):
    def test_interactive(self):
        conf = vidtrain.datahandler.Config(String='test', Int=5, Float=5.0)
        conf.new_item(name='Option', value=1, options=['foo', 'bar', 'baz'])
        conf.new_item(name='Dropdown', value=1, options=['food', 'bard', 'bazd'], gui_element_type='dropdown')
        conf_gui = vidtrain.gui.backend.ConfigPanel(master=self.root, config=conf)
        conf_gui.show()
        self.root.run()
        for name, item in conf.items():
            if isinstance(item, vidtrain.datahandler.interface.OptionConfigItem):
                print('name: {}, value: {}, chosen option: {}'.format(name, item.value, item.current_option))
            else:
                print('name: {}, value:{}'.format(name, item.value))
        # TODO: (prio 3) test that gui updates when the config value changes


class TestLinkedStacks(GUIWithData):
    def test_interactive(self):
        self.stack.load(self.stack_path)
        ls = vidtrain.gui.backend.LinkedStacks(self.root, [self.stack.data, self.stack.data])
        ls.show()
        self.root.run()


class TestJunctionExtraction(GUIWithData):
    def test_interactive(self):
        self.workflow_data.path = os.path.join(self.tempdir, 'teststack.tif')
        je = vidtrain.workflow.preparation.JunctionExtraction(self.root, workflow_data=self.workflow_data)
        je.load_input()
        je.show()
        self.root.run()

    def test_without_projections(self):
        for proj_file in glob.glob(os.path.join(self.eval_path, 'projection_*.tif')):
            os.remove(proj_file)
        self.workflow_data.path = os.path.join(self.tempdir, 'teststack.tif')
        je = vidtrain.workflow.preparation.JunctionExtraction(self.root, workflow_data=self.workflow_data)
        je.load_input()
        je.show()
        self.root.run()


class TestJunctionAnnotation(GUIBase):
    def test_interactive(self):
        self.workflow_data.processed.load(os.path.join(testdir, 'junction_extraction_output.npz'))
        ja = vidtrain.workflow.annotation.JunctionAnnotation(self.root, self.workflow_data)
        ja.show()
        self.root.run()


class TestTrainNetwork(GUIBase):
    def test_interactive(self):
        self.workflow_data.processed.load(os.path.join(testdir, 'junction_extraction_output.npz'))
        self.workflow_data.annotation.update_multi_stack(self.workflow_data.processed)
        tn = vidtrain.workflow.training.JunctionNetworkTraining(self.root, self.workflow_data)
        tn.show()
        self.root.run()


class TestJunctionPrediction(GUIWithData):
    def setUp(self):
        super().setUp()
        shutil.rmtree(self.eval_path, ignore_errors=True)
        shutil.copytree(os.path.join(testdir, 'kin1_eval'), self.eval_path)
        self.workflow_data.processed.load(os.path.join(self.eval_path, 'junction_stacks.npz'))
        self.workflow_data.networks.load(path_prefix=os.path.join(self.eval_path, 'network'))

    def test_interactive(self):
        pr = vidtrain.workflow.prediction.JunctionPrediction(self.root, self.workflow_data)
        pr.show()
        self.root.run()

    def test_load_input(self):
        pr = vidtrain.workflow.prediction.JunctionPrediction(self.root, self.workflow_data)
        pr.load_input()
        pr.show()
        self.root.run()

    def test_autosave(self):
        pr = vidtrain.workflow.prediction.JunctionPrediction(self.root, self.workflow_data)
        pr.autosave = True
        pr.update()
        pr.show()
        self.root.run()

    def test_scaled_interactive(self):
        self.scaler = micdata.formatting.StackScaler(target_dim=16)
        for key, stack in enumerate(self.workflow_data.processed):
            self.workflow_data.processed.data[key] = self.scaler.apply(stack)
        pr = vidtrain.workflow.prediction.JunctionPrediction(self.root, self.workflow_data)
        pr.show()
        self.root.run()


class TestTabs(GUIBase):
    def test_interactive(self):
        tabs = vidtrain.gui.backend.Tabs(self.root)
        panel = vidtrain.gui.backend.Panel(tabs.tabs)
        panel.add_label('test')
        panel2 = vidtrain.gui.backend.Panel(tabs.tabs)
        panel2.add_label('test2')
        tabs.show()
        tabs.tabs.add(panel, text='t')
        tabs.tabs.add(panel2, text='t2')
        self.root.run()


class TestJunctionEvaluation(GUIWithData):
    def setUp(self):
        super().setUp()
        self.temppath = tempfile.mkdtemp()
        self.eval_path = os.path.join(self.temppath, 'kin1_eval')
        try:
            shutil.copytree(os.path.join(testdir, 'kin1-pred_eval'), self.eval_path)
        except FileExistsError:
            pass
        self.workflow_data.path = os.path.join(self.temppath, 'kin1')

    def tearDown(self):
        super().tearDown()
        shutil.rmtree(self.temppath, ignore_errors=True)

    def test_interactive(self):
        je = vidtrain.workflow.evaluation.JunctionEvaluation(self.root, self.workflow_data)
        je.load_input()
        je.show()
        self.root.run()

    def test_annotation_only(self):
        je = vidtrain.workflow.evaluation.JunctionEvaluation(self.root, self.workflow_data)
        for file in glob.glob(os.path.join(self.eval_path, 'junction_predicted*')):
            os.remove(file)
        je.load_input()
        je.show()
        self.root.run()

    def test_incomplete_data(self):
        je = vidtrain.workflow.evaluation.JunctionEvaluation(self.root, self.workflow_data)
        for file in glob.glob(os.path.join(self.eval_path, 'junction_predicted_manual*')):
            os.remove(file)
        je.load_input()
        je.show()
        self.root.run()

    def test_inconsistent_data(self):
        shutil.rmtree(self.eval_path, ignore_errors=True)
        try:
            shutil.copytree(os.path.join(testdir, 'kin1_eval'), self.eval_path)
        except FileExistsError:
            pass
        je = vidtrain.workflow.evaluation.JunctionEvaluation(self.root, self.workflow_data)
        je.load_input()
        je.show()
        self.root.run()


class TestJunctionAnalysis(unittest.TestCase):
    def setUp(self):
        self.temppath = tempfile.mkdtemp()
        self.stack_path = os.path.join(self.temppath, 'teststack.tif')
        shutil.copyfile(os.path.join(testdir, 'teststack.tif'), self.stack_path)

    def tearDown(self):
        shutil.rmtree(self.temppath, ignore_errors=True)

    def test_stack_only(self):
        ja = vidtrain.workflow.JunctionAnalysis(path=self.stack_path)
        ja.run()

    def test_with_saved_data(self):
        try:
            shutil.copytree(os.path.join(testdir, 'teststack_eval'), os.path.join(self.temppath, 'teststack_eval'))
        except Exception:
            pass
        ja = vidtrain.workflow.JunctionAnalysis(path=self.stack_path)
        ja.run()

    def test_with_trained_data(self):
        try:
            shutil.copytree(os.path.join(testdir, 'kin1_eval'), os.path.join(self.temppath, 'teststack_eval'))
        except Exception:
            pass
        ja = vidtrain.workflow.JunctionAnalysis(path=self.stack_path)
        ja.run()


if __name__ == '__main__':
    unittest.main()
