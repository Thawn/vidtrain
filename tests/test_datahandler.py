import re
import glob
import tensorflow.keras as keras
import micdata
import time
import tempfile
import unittest
import sys
import os
import shutil
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import tifffile
try:
    from vidtrain import datahandler
except ModuleNotFoundError:
    sys.path.append(os.path.dirname(os.path.dirname(__file__)))
    from vidtrain import datahandler

params = {'name': 'test',
          'int_val': 5,
          'float_val': 0.1,
          'str_val': 'value'}

testdir = os.path.dirname(__file__)
# TODO: (prio 4) add tests for empty() method


class TestCreateConfigItem(unittest.TestCase):
    def test_str(self):
        ci = datahandler.create_config_item(name=params['name'], value=params['str_val'])
        self.assertIsInstance(ci, datahandler.interface.ConfigItem)
        self.assertIsInstance(ci, datahandler.config.StringConfigItem)

    def test_float(self):
        ci = datahandler.create_config_item(name=params['name'], value=params['float_val'])
        self.assertIsInstance(ci, datahandler.interface.NumericConfigItem)
        self.assertIsInstance(ci, datahandler.config.FloatConfigItem)

    def test_int(self):
        ci = datahandler.create_config_item(name=params['name'], value=params['int_val'])
        self.assertIsInstance(ci, datahandler.interface.NumericConfigItem)
        self.assertIsInstance(ci, datahandler.config.IntConfigItem)


class ChangeCallback():
    def __init__(self):
        self.changed = False
        self.value = None

    def callback(self, value):
        self.changed = True
        self.value = value


class TestStringConfigItem(unittest.TestCase):
    def setUp(self):
        self.name = params['name']
        self.value = params['str_val']
        self.el_t = 'entry'
        self.val_type = str
        self.ci = datahandler.config.StringConfigItem(name=self.name, value=self.value)
        self.wrong_val = params['int_val']
        self.checked_val = '5'

    def test_init(self):
        self.assertEqual(self.ci.name, self.name)
        self.assertEqual(self.ci.value, self.value)
        self.assertEqual(self.ci.default_value, self.value)
        self.assertEqual(self.ci.gui_element_type, self.el_t)
        self.assertIn(self.ci.gui_element_type, self.ci.GUI_ELEMENT_TYPES)
        self.assertIsInstance(self.ci.value, self.val_type)
        self.assertIsInstance(self.ci.default_value, self.val_type)

    def test_convert_val(self):
        self.assertIsInstance(self.ci.convert_val(self.wrong_val), self.val_type)
        self.assertAlmostEqual(self.ci.convert_val(self.wrong_val), self.checked_val)

    def test_on_change(self):
        cb = ChangeCallback()
        self.ci.on_change = cb.callback
        self.ci.value = self.checked_val
        self.assertTrue(cb.changed)
        self.assertAlmostEqual(cb.value, self.checked_val)

    def test_import_export(self):
        # TODO: (prio 4)
        pass


class TestOptionConfigItem(TestStringConfigItem):
    def setUp(self):
        self.name = params['name']
        self.value = 1
        self.el_t = 'spinbox'
        self.val_type = int
        self.options = ['foo', 'bar', 'baz']
        self.ci = datahandler.config.OptionConfigItem(name=self.name, value=self.value, options=self.options)
        self.wrong_val = '5'
        self.checked_val = 2
        self.not_in_options = 'bla'

    def test_current_option(self):
        self.assertEqual(self.ci.current_option, self.options[self.value])
        self.ci.current_option = self.options[self.checked_val]
        self.assertEqual(self.ci.value, self.checked_val)
        with self.assertRaises(ValueError):
            self.ci.current_option = self.not_in_options

    def test_toggle(self):
        self.ci.toggle()
        self.assertEqual(self.ci.value, self.value + 1)
        self.ci.toggle()
        self.assertEqual(self.ci.value, 0)


class TestIntConfigItem(TestStringConfigItem):
    def setUp(self):
        super().setUp()
        self.value = params['int_val']
        self.el_t = 'slider'
        self.val_type = int
        self.inc = 1
        self.f = 0
        self.t = 100
        self.ci = datahandler.config.IntConfigItem(name=self.name, value=self.value)
        self.wrong_val = '2.1'
        self.checked_val = 2
        self.other_inc = 3
        self.other_checked_val = 3
        self.wrong_to = 20.3
        self.correct_to = 20
        self.high_val = 25
        self.wrong_from = 2.9
        self.correct_from = 3
        self.low_val = 1
        self.rounded_val = 0
        self.other_high_val = 25
        self.high_rounded_val = 24

    def test_init(self):
        super().test_init()
        self.assertEqual(self.ci.increment, self.inc)
        self.assertEqual(self.ci.from_, self.f)
        self.assertEqual(self.ci.to, self.t)
        self.assertIsInstance(self.ci.increment, self.val_type)
        self.assertIsInstance(self.ci.from_, self.val_type)
        self.assertIsInstance(self.ci.to, self.val_type)

    def test_convert_val(self):
        super().test_convert_val()
        self.ci.increment = self.other_inc
        self.assertAlmostEqual(self.ci.convert_val(self.wrong_val), self.other_checked_val)

    def test_to(self):
        self.ci.to = self.wrong_to
        self.assertAlmostEqual(self.ci.to, self.correct_to)
        self.ci.value = self.high_val
        self.assertAlmostEqual(self.ci.value, self.correct_to)

    def test_from_(self):
        self.ci.from_ = self.wrong_from
        self.assertAlmostEqual(self.ci.from_, self.correct_from)
        self.ci.value = self.low_val
        self.assertAlmostEqual(self.ci.value, self.correct_from)

    def test_increment(self):
        self.ci.increment = self.other_inc
        self.assertAlmostEqual(self.ci.value % self.ci.increment, self.rounded_val)
        self.ci.value = self.other_high_val
        self.assertEqual(self.ci.value, self.high_rounded_val)


class TestFloatConfigItem(TestIntConfigItem):
    def setUp(self):
        super().setUp()
        self.value = params['float_val']
        self.el_t = 'slider'
        self.val_type = float
        self.inc = 0.1
        self.f = 0
        self.t = 10
        self.ci = datahandler.config.FloatConfigItem(name=self.name, value=self.value)
        self.wrong_val = '1.94'
        self.checked_val = 1.9
        self.other_inc = 1.5
        self.other_checked_val = 1.5
        self.wrong_to = 20.37
        self.correct_to = 20.4
        self.high_val = 25
        self.wrong_from = 2.91
        self.correct_from = 2.9
        self.low_val = 1
        self.rounded_val = 0
        self.other_high_val = 8
        self.high_rounded_val = 7.5


class TestConfig(unittest.TestCase):
    def setUp(self):
        self.d = {'test': 'val', 'int': 5}

    def test_init(self):
        c = datahandler.Config()
        self.assertIsInstance(c, datahandler.interface.Config)

    def test_init_dict(self):
        c = datahandler.Config(self.d)
        self.is_dict(c)

    def test_init_kw(self):
        c = datahandler.Config(**self.d)
        self.is_dict(c)

    def test_update(self):
        c = datahandler.Config()
        c.update(self.d)

    def test_new_item(self):
        c = datahandler.Config()
        c.new_item('test', 'val')
        c.new_item('int', 5, from_=1, to=101, increment=5)
        self.is_dict(c)

    def is_dict(self, list_):
        try:
            iter(list_)
        except TypeError:
            self.fail('{} is not iterable.'.format(list_))
        for name, item in list_.items():
            with self.subTest(name=name, item=item):
                self.assertIsInstance(item, datahandler.interface.ConfigItem)
        self.assertIsInstance(list_['test'], datahandler.interface.ConfigItem)


class TestRotatable(unittest.TestCase):
    def setUp(self):
        self.rotable = datahandler.rotatable.RotatableMixin()

    def test_rot(self):
        self.assertIsInstance(self.rotable.rot, int)
        for i in range(0, 6):
            with self.subTest(i=i):
                self.rotable.rot = i
                self.assertEqual(self.rotable.rot, i % 4)

    def test_flipped(self):
        self.assertIsInstance(self.rotable.flipped, bool)

    def test_rotate(self):
        for i in range(0, 6):
            with self.subTest(i=i):
                self.assertEqual(self.rotable.rot, i % 4)
                self.rotable.rotate()

    def test_flip(self):
        flipped = self.rotable.flipped
        self.rotable.flip()
        self.assertNotEqual(flipped, self.rotable.flipped)


class TestRotatableRectangle(unittest.TestCase):
    def setUp(self):
        self.image_dimensions = (512, 512)
        self.size = 64
        self.pos = (64, 16)
        self.solutions = [(64, 16), (16, 384), (384, 432), (432, 64), (64, 16)]
        self.flip_solution = (384, 16)
        self.rect = datahandler.rotatable.RotatableRectangle((0, 0), self.size, self.image_dimensions)

    def test_dim(self):
        self.rect.dim = 16
        self.assertEqual(self.rect.dim, (16, 16))
        self.rect.dim = (24, 16)
        self.assertEqual(self.rect.dim, (24, 16))
        self.rect.rotate()
        self.assertEqual(self.rect.dim, (16, 24))
        self.rect.rotate()
        self.assertEqual(self.rect.dim, (24, 16))
        self.rect.rotate()
        self.assertEqual(self.rect.dim, (16, 24))

    def test_init_asymmetric_size(self):
        r = datahandler.rotatable.RotatableRectangle((0, 0), (16, 24), self.image_dimensions)
        self.assertEqual(r.rect._width, 16)
        self.assertEqual(r.rect._height, 24)

    def test_rect(self):
        self.assertIsInstance(self.rect.rect, matplotlib.patches.Rectangle)

    def test_pos(self):
        self.rect.pos = self.pos
        self.assertEqual(self.rect.pos, self.pos)
        self.rect.rotate()
        self.rect.flip()
        self.assertEqual(self.rect.pos, self.rect._rotate_coords(self.pos))
        self.rect.pos = self.pos
        self.assertEqual(self.rect.pos, self.pos)

    def test_raw_pos(self):
        self.rect.pos = self.pos
        self.assertEqual(self.rect.raw_pos, self.pos)
        self.rect.rotate()
        self.rect.flip()
        self.assertEqual(self.rect.raw_pos, self.pos)
        self.rect.pos = self.pos
        self.assertEqual(self.rect.raw_pos, self.rect._unrotate_coords(self.pos))

    def test_update(self):
        self.rect.rotate()
        self.assertEqual(self.rect.rect.get_xy(), self.rect.raw_pos)
        self.rect.update()
        self.assertNotEqual(self.rect.rect.get_xy(), self.rect.raw_pos)
        self.assertEqual(self.rect.rect.get_xy(), self.rect.pos)

    def test_draw(self):
        self._make_figure()
        self.assertIsNone(self.rect.rect.axes)
        self.rect.draw(self.im_obj.axes)
        self.assertIsNotNone(self.rect.rect.axes)

    def test_remove(self):
        self._make_figure()
        self.im_obj.axes.add_patch(self.rect.rect)
        self.assertIsNotNone(self.rect.rect.axes)
        self.rect.remove()
        self.assertIsNone(self.rect.rect.axes)

    def test_rotate_coords(self):
        for i in range(0, 4):
            with self.subTest(i=i):
                self.assertEqual(self.rect._rotate_coords(self.pos), self.solutions[i])
                self.rect.rotate()
        self.rect.flip()
        self.assertEqual(self.rect._rotate_coords(self.pos), self.flip_solution)

    def test_unrotate_coords(self):
        for i in range(0, 4):
            with self.subTest(i=i):
                self.assertEqual(self.rect._unrotate_coords(self.pos), self.solutions[4 - i])
                self.rect.rotate()
        self.rect.flip()
        self.assertEqual(self.rect._rotate_coords(self.pos), self.flip_solution)

    def test_flip_coords(self):
        flipped = self.rect._flip_coords(self.pos)
        self.assertEqual(flipped, self.flip_solution)

    def test_value_rotation(self):
        for i in range(0, 6):
            with self.subTest(i=i):
                if i > 4:
                    self.assertRaises(ValueError, self.rect._value_rotation, self.pos, i)
                else:
                    self.assertEqual(self.rect._value_rotation(self.pos, i), self.solutions[i])

    def _make_figure(self):
        self.im_obj = plt.imshow(np.random.normal(size=self.image_dimensions))


class TestRectangleList(unittest.TestCase):
    def setUp(self):
        self.image_shape = (1024, 512)
        self.size = 16
        self.flipped = True
        self.rot = 1
        self.rectangles = datahandler.rotatable.RectangleList()
        self.pos = (1, 10)
        self.rectangles.add_rect(self.pos)

    def test_add_rect(self):
        numrect = len(self.rectangles.rect_list)
        self.rectangles.add_rect(self.pos)
        self.assertEqual(len(self.rectangles.rect_list), numrect + 1)
        self.assertIsInstance(self.rectangles.rect_list[-1], datahandler.rotatable.RotatableRectangle)
        self.assertEqual(self.rectangles.rect_list[-1].pos, self.pos)

    def test_delete_rect(self):
        numrect = len(self.rectangles.rect_list)
        self.rectangles.delete_rect()
        self.assertEqual(len(self.rectangles.rect_list), numrect - 1)

    # TODO (prio 3): def test_attrs(self): def test_rotate(self): def test_flip(self): def test_remove_all(self):


class TestRectangleMatrix(unittest.TestCase):
    def setUp(self):
        self.image_shape = (512, 512)
        self.num_rows = 2
        self.num_cols = 5
        self.row_height = 18
        self.flipped = True
        self.rot = 1
        self.size = (8, 8)
        self.positions = [(100, -10), (200.0, -20.0), (300.0, -30.0), (400.0, -40.0), (98.20893305762202, -27.910669423779837), (198.208933057622, -37.91066942377984),
                          (298.208933057622, -47.91066942377984), (398.208933057622, -57.91066942377984), (498.208933057622, -67.91066942377984), (500, -50)]
        self.rectangles = datahandler.rotatable.RectangleMatrix(
            image_shape=self.image_shape, num_rows=self.num_rows, num_cols=self.num_cols, row_height=self.row_height, rot=self.rot, flipped=self.flipped, size=self.size)

    def test_init(self):
        self.assertIsInstance(self.rectangles, datahandler.interface.Rotatable)
        self.assertEqual(self.rectangles.image_shape, self.image_shape)
        self.assertEqual(self.rectangles.num_rows, self.num_rows)
        self.assertEqual(self.rectangles.num_cols, self.num_cols)
        self.assertAlmostEqual(self.rectangles.row_height, self.row_height)
        self.assertEqual(self.rectangles.flipped, self.flipped)
        self.assertEqual(self.rectangles.rot, self.rot)
        self.assertEqual(self.rectangles.dim, self.size)

    def test_generator(self):
        rect = next(self.rectangles.generator())
        self.assertIsInstance(rect, datahandler.interface.Rotatable)
        for rect in self.rectangles.generator():
            with self.subTest(rect=rect):
                self.assertIsInstance(rect, datahandler.interface.Rotatable)

    def test_update_rect_number(self):
        self.rectangles.num_cols += 1
        self.rectangles.num_rows += 1
        self.assertNotEqual(len(self.rectangles.rect_list), self.rectangles.num_cols * self.rectangles.num_rows)
        self.rectangles.update_rect_number()
        self.assertEqual(len(self.rectangles.rect_list), self.rectangles.num_cols * self.rectangles.num_rows)

    def test_distribute(self):
        self.rectangles.left_rect.pos = self.positions[0]
        self.rectangles.right_rect.pos = self.positions[-1]
        self.rectangles.distribute()
        self._verify_positions()

    def test_current_rect(self):
        self.rectangles.select_left()
        self.rectangles.store_current_pos(self.positions[0])
        self.assertEqual(self.rectangles.left_rect.pos, self.positions[0])
        self.rectangles.select_right()
        self.rectangles.store_current_pos(self.positions[1])
        self.assertEqual(self.rectangles.right_rect.pos, self.positions[1])
        self.rectangles.toggle_corner()
        self.assertEqual(self.rectangles.current_rect, self.rectangles.left_rect)

    def test_from_positions(self):
        self.rectangles.from_positions(self.positions)
        self._verify_positions()
        self.test_init()
        self.rectangles.num_cols += 5
        self.rectangles.num_rows += 1
        self.rectangles.update_rect_number()
        self.rectangles.row_height += 1
        self.rectangles.from_positions(self.positions)
        self._verify_positions()
        self.test_init()

    def _verify_positions(self):
        positions = []
        for i, rect in enumerate(self.rectangles.generator()):
            if i == 0 or i == self.rectangles.num_cols - 1:
                positions.append(rect.pos)
            with self.subTest(i=i):
                self.assertEqual(self.positions[i], rect.pos)
        self.assertAlmostEqual(self.rectangles.row_height, np.linalg.norm(
            np.array(positions[0]) - np.array(positions[1])))


class TestImageStack(unittest.TestCase):
    def setUp(self):
        self.path = os.path.join(testdir, 'teststack.tif')
        self.temppath = os.path.join(tempfile.gettempdir(), tempfile.gettempprefix() + '.tif')
        self.images = micdata.formatting.StackNormalizer().apply(micdata.io.IOFactory(self.path).load())
        self.stack = datahandler.ImageStack(self.images)
        self.shape = (10, 512, 512, 1)
        self.tile_dim = (32, 32)
        self.tile_shape = (10, 32, 32, 1)

    def tearDown(self):
        try:
            os.remove(self.temppath)
        except FileNotFoundError:
            pass

    def test_load(self):
        st = datahandler.ImageStack()
        self.assertIsNone(st.data)
        st.load(self.path)
        self.assertEqual(st.data.shape, (10, 512, 512, 1))
        self.assertTrue(np.array_equal(np.squeeze(st.data), np.squeeze(self.images)))

    def test_props(self):
        self.assertEqual(self.stack.num_slices, 10)
        self.assertEqual(self.stack.width, 512)
        self.assertEqual(self.stack.height, 512)
        self.assertEqual(self.stack.image_shape, self.shape[1:])
        self.assertEqual(self.stack.num_channels, 1)

    def test_save(self):
        self.stack.save(self.temppath)
        read = tifffile.imread(self.temppath)
        self.assertTrue(np.array_equal(read, self.stack.data))

    def test_median(self):
        self.assertTrue(np.array_equal(self.stack.median(), np.squeeze(np.median(self.images, axis=0))))

    def test_mean(self):
        self.assertTrue(np.array_equal(self.stack.mean(), np.squeeze(np.mean(self.images, axis=0))))

    def test_std(self):
        self.assertTrue(np.array_equal(self.stack.std(), np.squeeze(np.std(self.images, axis=0))))

    def test_max(self):
        self.assertTrue(np.array_equal(self.stack.max(), np.squeeze(np.max(self.images, axis=0))))

    def test_copy_tile(self):
        tile = self.stack.copy_tile((0, 0), self.tile_dim)
        self.assertEqual(tile.shape, self.tile_shape)
        tile = self.stack.copy_tile((-33, -33), self.tile_dim)
        self.assertEqual(tile.shape, self.tile_shape)
        with self.assertRaises(IndexError):
            self.stack.copy_tile((0, 511), self.tile_dim)
        with self.assertRaises(IndexError):
            self.stack.copy_tile((511, 0), self.tile_dim)
        with self.assertRaises(IndexError):
            self.stack.copy_tile((-32, 0), self.tile_dim)
        with self.assertRaises(IndexError):
            self.stack.copy_tile((0, -32), self.tile_dim)
    # TODO: test cache


class TestMultiImageStack(unittest.TestCase):
    def setUp(self):
        self.path = os.path.join(testdir, 'junction_extraction_output.npz')
        self.temppath = os.path.join(tempfile.gettempdir(), tempfile.gettempprefix() + '.npz')
        self.ms = datahandler.MultiImageStack()
        self.ms.load(self.path)
        self.shape = (31, 10, 16, 16, 1)
        self.im_shape = (10, 16, 16, 1)
        self.image_stack_class = datahandler.interface.ImageStack
        self.valid_data = np.random.normal(self.im_shape)
        self.valid_positions = [(5, 5), 'x:5;y:5', '(5, 5)']
        self.invalid_data = 5
        self.invalid_position = 'position'

    def tearDown(self):
        try:
            os.remove(self.temppath)
        except FileNotFoundError:
            pass

    def _verify_data(self):
        self.assertTrue(np.array_equal(self.ms[0], self.valid_data))
        self.assertEqual(self.ms.positions[0], self.valid_positions[0])

    def test_init(self):
        ms = datahandler.MultiImageStack()
        self.assertIsInstance(ms, datahandler.interface.MultiImageStack)
        self.assertIsInstance(self.ms, datahandler.interface.MultiImageStack)
        l = list(range(10))
        ms = datahandler.MultiImageStack(*l)
        self.assertListEqual(ms.data, l)
        self.assertEqual(len(ms.positions), len(ms.data))
        self.assertIsNone(ms.positions[0])
        self.assertIsInstance(self.ms.positions[0], tuple)

    def test_getitem(self):
        self.assertIsInstance(self.ms[0], np.ndarray)
        self.assertEqual(self.ms.names[1], 'x:0475 y:0349')

    def test_setitem(self):
        for pos in self.valid_positions:
            with self.subTest(pos=pos):
                self.ms[0] = (self.valid_data, pos)
                self._verify_data()
        self.ms[0] = (self.valid_data, self.invalid_position)
        self.assertIsNone(self.ms.positions[0])
        with self.assertRaises(AssertionError):
            self.ms[0] = self.valid_data
        with self.assertRaises(AssertionError):
            self.ms[0] = (self.invalid_data, self.valid_positions[0])

    def test_insert(self):
        for pos in self.valid_positions:
            with self.subTest(pos=pos):
                l = len(self.ms)
                self.ms.insert(0, (self.valid_data, pos))
                self._verify_data()
                self.assertTrue(len(self.ms) > l)
        self.ms[0] = (self.valid_data, self.invalid_position)
        self.assertIsNone(self.ms.positions[0])
        with self.assertRaises(AssertionError):
            self.ms.insert(0, self.valid_data)
        with self.assertRaises(AssertionError):
            self.ms.insert(0, (self.invalid_data, self.valid_positions[0]))

    def test_add(self):
        l = len(self.ms)
        self.ms = self.ms + self.ms
        self.assertEqual(len(self.ms), l * 2)
        self.assertTrue(np.array_equal(self.ms[0], self.ms[l]))
        self.assertEqual(self.ms.positions[0], self.ms.positions[l])

    def test_append(self):
        for n in range(3):
            self.ms.append(np.zeros(self.im_shape), (n, n))
        self.assertEqual(len(self.ms), self.shape[0] + 3)
        self.assertEqual(self.ms.positions[-1], (2, 2))
        self.assertEqual(self.ms[-1].shape, self.im_shape)

    def test_np_array(self):
        self.assertEqual(self.ms.np_array().shape, self.shape)

    def test_image_stacks(self):
        im_st = next(self.ms.image_stacks())
        self.assertIsInstance(im_st, self.image_stack_class)
        self.assertEqual(im_st.data.shape, self.im_shape)
        for n, stack in enumerate(self.ms.image_stacks()):
            with self.subTest(n=n):
                self.assertIsInstance(stack, self.image_stack_class)

    def test_save_load(self):
        self.ms.save(self.temppath)
        loader = datahandler.MultiImageStack()
        loader.load(self.temppath)
        self.assertListEqual(self.ms.positions, loader.positions)
        self.assertTrue(np.array_equal(self.ms[0][0], loader[0][0]))

    def test_copy(self):
        copy = self.ms.copy()
        self.assertIsInstance(copy, datahandler.interface.MultiImageStack)
        k = 0
        self.assertTrue(np.array_equal(copy[k], self.ms[k]))
        copy[k][0, 0] = 5
        self.assertFalse(np.array_equal(copy[k], self.ms[k]))
        self.assertListEqual(self.ms.positions, copy.positions)
        copy.positions[k] = (0, 0)
        self.assertNotEqual(self.ms.positions, copy.positions)


class TestMultiImageStackClassification(TestMultiImageStack):
    def setUp(self):
        super().setUp()
        self.path = os.path.join(testdir, 'junction_extraction_output.npz')
        self.temppath = os.path.join(tempfile.gettempdir(), tempfile.gettempprefix() + '.npz')
        self.m = datahandler.MultiImageStack()
        self.m.load(self.path)
        self.ms = datahandler.MultiImageStackClassification(multi_image_stack=self.m)
        self.shape = (31, 10, 6)
        self.im_shape = (10, 6)
        self.image_stack_class = datahandler.interface.ImageStackClassification
        self.default_num_cat = 6
        self.changed_num_cat = 7
        self.changed_shape = (10, 7)

    def test_num_categories(self):
        self.ms.all_category_names = ['Changed']
        copy = self.ms.copy()
        self.assertEqual(copy.all_category_names, self.ms.all_category_names)
        k = 0
        copy[k][0, 0] = 1
        copy.num_categories = self.changed_num_cat
        self.assertEqual(self.ms.num_categories, self.default_num_cat)
        self.assertEqual(copy[k].shape, self.changed_shape)
        self.assertEqual(copy[k][0, 0], 1)
        copy[k][0, self.default_num_cat] = 1
        with self.assertRaises(datahandler.interface.VidtrainDataLossException):
            copy.num_categories = self.default_num_cat
        copy.force = True
        copy.num_categories = self.default_num_cat

    def test_update_multi_stack(self):
        copy = self.ms.copy()
        copy.data[0] = 5
        copy.positions[0] = (5, 5)
        copy.append(6, (1, 2))
        self.assertNotEqual(copy.positions[0], self.m.positions[0])
        copy.update_multi_stack(self.m)
        self.assertEqual(copy[0], 5)
        self.assertEqual(copy.positions[0], self.m.positions[0])
        for i, data in enumerate(copy[1:]):
            with self.subTest(i=i):
                self.assertTrue(np.array_equal(data, self.ms[i]))
        del(copy[-1])
        del(copy[-1])
        self.assertNotEqual(len(copy), len(self.ms))
        copy.update_multi_stack(self.m)
        self.assertEqual(len(copy), len(self.ms))
        self.assertEqual(copy[0], 5)


class TestImageStackClassification(unittest.TestCase):
    def setUp(self):
        self.temppath = os.path.join(tempfile.gettempdir(), tempfile.gettempprefix() + '.npz')
        self.shape = (10, 4)
        self.data = np.zeros(self.shape)
        self.ms = datahandler.ImageStackClassification(self.data)
        self.categories = ['A', 'B', 'C', 'D', 'E']

    def tearDown(self):
        try:
            os.remove(self.temppath)
        except FileNotFoundError:
            pass

    def test_init(self):
        ms = datahandler.ImageStackClassification()
        self.assertIsInstance(ms, datahandler.interface.ImageStackClassification)
        self.assertIsInstance(self.ms, datahandler.interface.ImageStackClassification)

    def test_num_categories(self):
        self.assertEqual(self.ms.num_categories, self.shape[1])
        self.assertEqual(self.ms.data.shape, self.shape)
        self.ms.data[1, 1] = 1
        self.ms.num_categories = 6
        self.assertEqual(self.ms.data.shape, (10, 6))
        self.assertEqual(self.ms.data[1, 1], 1)
        self.ms.num_categories = 2
        self.assertEqual(self.ms.data.shape, (10, 2))
        self.assertEqual(self.ms.data[1, 1], 1)
        self.ms.num_categories = 6
        self.ms.data[1, 5] = 1
        with self.assertRaises(datahandler.interface.VidtrainDataLossException):
            self.ms.num_categories = 5
        self.assertEqual(self.ms.data[1, 5], 1)
        self.ms.force = True
        self.ms.num_categories = 5

    def test_category_names(self):
        gen = self.ms.category_names
        for n in range(self.ms.num_categories):
            with self.subTest(n=n):
                self.assertEqual(next(gen), 'cat {}'.format(n))
        self.ms.category_names = self.categories
        self.assertEqual(len(list(self.ms.category_names)), self.ms.num_categories)
        for n, cat in enumerate(self.ms.category_names):
            with self.subTest(n=n):
                self.assertEqual(cat, self.categories[n])
        self.ms.num_categories = 5
        self.assertEqual(len(list(self.ms.category_names)), self.ms.num_categories)
        for n, cat in enumerate(self.ms.category_names):
            with self.subTest(n=n):
                self.assertEqual(cat, self.categories[n])

    def test_save_load(self):
        self.ms.save(self.temppath)
        loader = datahandler.ImageStackClassification()
        loader.load(self.temppath)
        self.assertTrue(np.array_equal(self.ms.data, loader.data))


class TestTrainingDataGenerator(unittest.TestCase):
    def setUp(self):
        self.path = os.path.join(testdir, 'junction_extraction_output.npz')
        self.temppath = os.path.join(tempfile.mkdtemp(), tempfile.gettempprefix())
        self.slicer = datahandler.data.FormatTrainingData(slice_len=10, slice_padding=0)
        self.ms = datahandler.MultiImageStack()
        self.ms.load(self.path)
        self.ann = datahandler.MultiImageStackClassification(multi_image_stack=self.ms)
        self.training_data = datahandler.models.TrainingDataGenerator(*self.slicer.apply(x=self.ms, y=self.ann))
        self.training_shape = (64, 16, 16, 1)
        self.annotation_shape = (64, 6)
        self.batch_size = 2

    def tearDown(self):
        shutil.rmtree(os.path.dirname(self.temppath), ignore_errors=True)

    def test_data_shape(self):
        self.assertEqual(self.training_data.data_shape, self.training_shape)
        self.assertEqual(self.training_data.annotation_shape, self.annotation_shape)

    def test_steps_per_epoch(self):
        self.assertEqual(self.training_data.steps_per_epoch(), 0)
        self.training_data.batch_size = 1
        self.assertEqual(self.training_data.steps_per_epoch(), 3)

    def test_validation_steps(self):
        self.assertEqual(self.training_data.validation_steps(), 0)
        self.training_data.batch_size = 1
        self.assertEqual(self.training_data.validation_steps(), 1)

    def test_generator(self):
        self.training_data.batch_size = self.batch_size
        (x, y) = next(self.training_data.generator())
        self.assertEqual(x.shape, (self.batch_size,) + self.training_shape)
        self.assertEqual(y.shape, (self.batch_size,) + self.annotation_shape)
        (x1, _) = next(self.training_data.generator())
        self.assertFalse(np.array_equal(x, x1))  # this may fail by chance p=1/191581231380566414401

    def test_validation_generator(self):
        self.training_data.batch_size = 1
        (x, y) = next(self.training_data.validation_generator())
        self.assertEqual(x.shape, (1,) + self.training_shape)
        self.assertEqual(y.shape, (1,) + self.annotation_shape)
        (x1, _) = next(self.training_data.validation_generator())
        self.assertFalse(np.array_equal(x, x1))  # this may fail by chance p=1/823543

    def test_save_load(self):
        self.training_data.len_sequences = 32
        self.training_data.batch_size = 5
        self.training_data.save(self.temppath)
        loader = datahandler.models.TrainingDataGenerator()
        loader.load(self.temppath)
        self.assertTrue(np.array_equal(np.array(self.training_data.x_train), np.array(loader.x_train)))
        self.assertEqual(self.training_data.len_sequences, loader.len_sequences)
        self.assertEqual(self.training_data.batch_size, loader.batch_size)


class TestSequenceSegmentationModel(unittest.TestCase):
    def setUp(self):
        self.temppath = os.path.join(tempfile.gettempdir(), tempfile.gettempprefix() + '.pkl.gz')
        self.ssm = datahandler.models.SequenceSegmentationModel()
        self.default_layers = 19
        self.default_kernels = 32
        self.default_kernels_last = 32
        self.more_conv3d = 3
        self.more_conv3d_layers = 22
        self.less_lstm = 3
        self.less_lstm_layers = 19
        self.less_kernels = 16
        self.less_kernels_last = 16
        self.first_conv_layer = 1
        self.first_lstm_layer = 3
        self.last_conv_layer = 15

    def tearDown(self):
        for f in glob.glob(self.temppath + '*'):
            os.remove(f)

    def test_compile(self):
        input_shape = (None, None, None, 1)
        model = self.ssm.compile(input_shape=input_shape)
        self.assertIsInstance(model, keras.models.Model)
        self.assertEqual(model.input_shape, (None,) + input_shape)
        self.assertEqual(len(model.layers), self.default_layers)
        self.assertEqual(model.layers[self.first_conv_layer].filters, self.default_kernels)
        self.assertEqual(model.layers[self.last_conv_layer].filters, self.default_kernels_last)
        self.assertIsInstance(model.optimizer, keras.optimizers.Adam)
        self.assertIsInstance(model.layers[self.first_lstm_layer].kernel_initializer,
                              keras.initializers.VarianceScaling)
        self.assertIsInstance(model.layers[self.first_lstm_layer].recurrent_initializer, keras.initializers.Orthogonal)
        with self.assertRaises(ValueError):
            model = self.ssm.compile(input_shape=(None, None, None, None))
        self.ssm.conv3d_layers = self.more_conv3d
        model2 = self.ssm.compile(input_shape=input_shape)
        self.assertEqual(len(model2.layers), self.more_conv3d_layers)
        self.ssm.convlstm2d_layers = self.less_lstm
        model2 = self.ssm.compile(input_shape=input_shape)
        self.assertEqual(len(model2.layers), self.less_lstm_layers)
        self.ssm.kernels = self.less_kernels
        model2 = self.ssm.compile(input_shape=input_shape)
        self.assertEqual(model2.layers[1].filters, self.less_kernels)
        self.assertEqual(model2.layers[15].filters, self.less_kernels)

    def test_save_load(self):
        self.ssm.conv3d_layers = self.more_conv3d
        self.ssm.kernels = self.less_kernels
        loader = datahandler.models.SequenceSegmentationModel()
        self.ssm.save(self.temppath)
        loader.load(self.temppath)
        loader.conv3d_layers = self.more_conv3d
        loader.kernels = self.less_kernels


class TestSequenceClassificationModel(TestSequenceSegmentationModel):
    def setUp(self):
        super().setUp()
        self.ssm = datahandler.models.SequenceClassificationModel()
        self.default_layers = 27
        self.default_kernels_last = 160
        self.more_conv3d_layers = 33
        self.less_lstm_layers = 30
        self.less_kernels_last = 80
        self.last_conv_layer = 23


class TestFileVersioning(unittest.TestCase):
    def setUp(self):
        self.path = os.path.join(testdir, 'teststack_eval', 'network.h5')
        self.other_path = os.path.join(testdir, 'teststack.tif')
        self.fv = datahandler.FileVersioning(self.path)
        self.file_list = [os.path.join(testdir, 'teststack_eval', 'network_2020-05-08_084405_weights.h5'),
                          os.path.join(testdir, 'teststack_eval', 'network_2020-05-08_084438_weights.h5')]

    def test_get_name(self):
        self.assertEqual(self.fv.get_name(unique=False), self.path)
        self.assertEqual(self.fv.get_name(self.other_path, unique=False), self.other_path)
        self.assertNotEqual(self.fv.get_name(), self.path)
        self.assertNotEqual(self.fv.get_name(self.other_path), self.other_path)
        self.assertIsNotNone(re.search(self.fv.TIMEFORMAT_REGEX, self.fv.get_name()))
        self.assertIsNotNone(re.search(self.fv.TIMEFORMAT_REGEX, self.fv.get_name(self.other_path)))

    def test_get_file_list(self):
        l = self.fv.get_file_list()
        self.assertEqual(l, self.file_list)
        l = self.fv.get_file_list(self.other_path)
        self.assertEqual(l, [self.other_path])

    def test_get_part_before_time(self):
        self.assertEqual(self.fv.get_part_before_time(self.fv.get_name()), os.path.splitext(self.path)[0])
        self.assertEqual(self.fv.get_part_before_time(), self.path)


class TestNetworkList(unittest.TestCase):
    def setUp(self):
        self.startTime = time.time()
        self.temppath = tempfile.mkdtemp()
        self.eval_dir = os.path.join(self.temppath, 'teststack_eval')
        shutil.copytree(os.path.join(testdir, 'teststack_eval'), self.eval_dir)
        self.path_prefix = os.path.join(self.eval_dir, 'network')
        self.stacks_path = os.path.join(self.eval_dir, 'junction_stacks.npz')
        self.network_list = datahandler.NetworkList()

    def tearDown(self):
        t = time.time() - self.startTime
        print('%s: %.3f' % (self.id(), t))
        shutil.rmtree(self.temppath, ignore_errors=True)

    def test_load(self):
        self.network_list.load(self.path_prefix)
        self.assertEqual(self.network_list.networks.shape, (2, 4))
        self.assertIsInstance(
            self.network_list.networks.loc[0, 'Network object'], datahandler.models.NetworkTrainer)

    def test_load_network(self):
        self.network_list.load_network(os.path.join(self.eval_dir, 'network_2020-05-08_084405'))
        self.assertAlmostEqual(self.network_list.networks.loc[0, 'Median loss'], 0.009, 4)
        self.assertIsInstance(self.network_list.networks.loc[0, 'Network object'], datahandler.models.NetworkTrainer)

    def test_del(self):
        self.network_list.load(self.path_prefix)
        self.assertEqual(len(self.network_list), 2)
        self.assertTrue(os.path.exists(os.path.join(self.eval_dir, 'network_2020-05-08_084438_weights.h5')))
        del self.network_list[0]
        self.assertEqual(len(self.network_list), 1)
        self.assertFalse(os.path.exists(os.path.join(self.eval_dir, 'network_2020-05-08_084438_weights.h5')))
        self.assertFalse(os.path.exists(os.path.join(self.eval_dir, 'network_2020-05-08_084438_model.pkl.gz')))
        self.assertFalse(os.path.exists(os.path.join(self.eval_dir, 'network_2020-05-08_084438_params.pkl.gz')))

    def test_apply_threshold(self):
        self.network_list.load(self.path_prefix)
        self.assertEqual(len(self.network_list), 2)
        self.network_list.apply_threshold(0.008)
        self.assertEqual(len(self.network_list), 1)
        self.assertTrue(all(self.network_list.networks['Median loss'] < 0.008))
        self.network_list.load(self.path_prefix)
        self.network_list.apply_threshold(0)
        self.assertEqual(len(self.network_list), 0)

    @ unittest.skip  # prediction does not work with junctionclassification network list yet
    def test_predict(self):
        self.network_list.load(self.path_prefix)
        x = datahandler.MultiImageStack()
        x.load(self.stacks_path)
        y = datahandler.MultiImageStackClassification(multi_image_stack=x)
        generator = datahandler.models.TrainingDataGenerator(x=x, y=y, batch_size=1)
        (y, _) = self.network_list.predict(generator, formatter=datahandler.PredictionFormatter(), best=2)
        self.assertEqual(y.shape, (3, 32, 12, 140, 140, 1))


class TestNetworkTrainer(unittest.TestCase):
    def setUp(self):
        self.temppath = os.path.join(tempfile.mkdtemp(), 'vidtrain')
        self.trainer = datahandler.models.NetworkTrainer()
        self.input_shape = (None, None, None, 1)
        self.num_categories = 1

    def tearDown(self):
        shutil.rmtree(os.path.dirname(self.temppath), ignore_errors=True)

    def test_compile_model(self):
        self.assertRaises(AssertionError, self.trainer.compile_model)  # need to set model_factory first
        self.trainer.model_factory = datahandler.models.SequenceSegmentationModel()
        self.assertRaises(AssertionError, self.trainer.compile_model)  # need to set input_shape first
        self.trainer.input_shape = self.input_shape
        self.assertRaises(AssertionError, self.trainer.compile_model)  # need to set num_categories first
        self.trainer.num_categories = self.num_categories
        self.trainer.compile_model()
        self.assertIsInstance(self.trainer.model, keras.models.Model)

    def test_draw_model(self):
        self.test_compile_model()
        drawing_file = self.temppath + '.svgz'
        self.trainer.draw_model(drawing_file)
        self.assertTrue(os.path.exists(drawing_file))
        self.assertAlmostEqual(os.path.getsize(drawing_file), 4500, -3)

    def test_train_save_load(self):
        with self.assertRaises(AssertionError):
            self.trainer.train()
        slicer = datahandler.data.FormatTrainingData(slice_len=10, slice_padding=0)
        ms = datahandler.MultiImageStack()
        ms.load(os.path.join(testdir, 'junction_extraction_output.npz'))
        ann = datahandler.MultiImageStackClassification(multi_image_stack=ms)
        training_data = datahandler.models.TrainingDataGenerator(*slicer.apply(x=ms, y=ann))
        training_data.batch_size = 1
        self.trainer.training_data = training_data
        self.trainer.epochs = 2
        with self.assertRaises(AssertionError):
            self.trainer.train()
        self.trainer.model_factory = datahandler.models.SequenceClassificationModel()
        with self.assertRaises(AssertionError):
            self.trainer.train()
        self.trainer.compile_model()
        self.trainer.train()
        self.trainer.save(self.temppath, unique=False)
        loader = datahandler.models.NetworkTrainer()
        loader.load(self.temppath)
        self.assertEqual(self.trainer.input_shape, loader.input_shape)
        self.assertIsInstance(loader.model_factory, type(self.trainer.model_factory))
        self.assertIsNotNone(loader.model)
        # self.assertEqual(self.trainer.model.weights, loader.model.weights)


class TestSequencePredictionFormatter(unittest.TestCase):
    def setUp(self):
        self.pred_f = datahandler.SequencePredictionFormatter()
        self.data = datahandler.MultiImageStack()
        self.data.from_dict(
            {'a': np.ones((140, 16, 16, 1)), 'b': np.ones((180, 16, 16, 1))})
        self.expected_shape = (7, 64, 16, 16, 1)
        self.prediction = np.ones((7, 64, 4))
        self.prediction[-1, -16:, :] = 0
        self.expected_shapes = ((140, 4), (180, 4))

    def test_apply(self):
        formatted = self.pred_f.apply(self.data)
        self.assertEqual(formatted.shape, self.expected_shape)

    def test_revert(self):
        self.pred_f.apply(self.data)
        reverted = self.pred_f.revert(self.data, self.prediction)
        self.assertIsInstance(reverted, datahandler.MultiImageStackClassification)
        self.assertListEqual(reverted.positions, self.data.positions)
        for i, shape in enumerate(self.expected_shapes):
            with self.subTest(key=i):
                self.assertEqual(reverted[i].shape, shape)
                self.assertEqual(reverted[i][-1, -1], 1.0)


class TestFormatTrainingData(unittest.TestCase):
    def setUp(self):
        self.ms = datahandler.MultiImageStack()
        self.ms.load(os.path.join(testdir, 'junction_extraction_output.npz'))
        self.ann = datahandler.MultiImageStackClassification(multi_image_stack=self.ms)
        self.target_dim = (32, 32)
        self.formatted_x_shape = (6, 16, 16, 1)
        self.formatted_y_shape = (6, 6)
        self.formatted_x_shape_scaled = (6, 32, 32, 1)

    def test_apply(self):
        ftd = datahandler.FormatTrainingData(slice_len=5, slice_padding=1)
        x, y = ftd.apply(self.ms, self.ann)
        self.assertEqual(len(x), len(y))
        self.assertEqual(x[1].shape, self.formatted_x_shape)
        self.assertEqual(y[1].shape, self.formatted_y_shape)

    def test_apply_scaled(self):
        ftd = datahandler.FormatTrainingData(slice_len=5, slice_padding=1, target_dim=(self.target_dim))
        x, y = ftd.apply(self.ms, self.ann)
        self.assertEqual(len(x), len(y))
        self.assertEqual(x[1].shape, self.formatted_x_shape_scaled)
        self.assertEqual(y[1].shape, self.formatted_y_shape)


@ unittest.skip
class TestJunctionAnalysisData(unittest.TestCase):
    # TODO: (prio 3)
    pass


class TestShiftAugmenter(unittest.TestCase):
    def setUp(self):
        self.load_data()
        self.aug = datahandler.models.ShiftAugmenter()
        self.random_vars = {'dx': 2, 'dy': -2}
        self.result = np.load(os.path.join(testdir, 'shifted_junction_extraction_output.npz'),
                              allow_pickle=False)['arr_0']
        self.var_init()
        self.target_shape = self.ms[0].shape

    def load_data(self):
        self.ms = datahandler.MultiImageStack()
        self.ms.load(os.path.join(testdir, 'junction_extraction_output.npz'))
        self.ann = datahandler.MultiImageStackClassification(multi_image_stack=self.ms)

    def var_init(self):
        for var, val in self.random_vars.items():
            setattr(self.aug, var, val)

    def test_shuffle(self):
        for var, val in self.random_vars.items():
            with self.subTest(var=var, val=val):
                while val == getattr(self.aug, var):
                    self.aug.shuffle()
                self.assertNotEqual(val, getattr(self.aug, var))

    def test_apply(self):
        a_x, a_y = self.aug.apply(self.ms[0], self.ann[0])
        self.verify_stack(a_x)
        self.verify_ann(a_y)

    def verify_stack(self, a_x):
        self.assertEqual(a_x.shape, self.target_shape)
        self.assertTrue(np.array_equal(a_x, self.result))

    def verify_ann(self, a_y):
        self.assertTrue(np.array_equal(a_y, self.ann[0]))


class TestRotateAugmenter(TestShiftAugmenter):
    def setUp(self):
        self.load_data()
        self.aug = datahandler.models.RotateAugmenter()
        self.random_vars = {'angle': 3}
        self.result = np.load(os.path.join(testdir, 'rotated_junction_extraction_output.npz'),
                              allow_pickle=False)['arr_0']
        self.var_init()
        self.target_shape = self.ms[0].shape


class TestScaleAugmenter(TestShiftAugmenter):
    def setUp(self):
        self.load_data()
        self.aug = datahandler.models.ScaleAugmenter()
        self.random_vars = {'scale': 1.5}
        self.result = np.load(os.path.join(testdir, 'scaled_junction_extraction_output.npz'),
                              allow_pickle=False)['arr_0']
        self.var_init()
        self.target_shape = (10, 24, 24, 1)


class TestFPSAugmenter(TestShiftAugmenter):
    def setUp(self):
        self.load_data()
        self.aug = datahandler.models.FPSAugmenter()
        self.random_vars = {'step': 2}
        self.result = np.load(os.path.join(testdir, 'fps_junction_extraction_output.npz'),
                              allow_pickle=False)['arr_0']
        self.var_init()
        self.target_shape = (5, 16, 16, 1)
        self.target_ann_shape = (5, 6)

    def verify_ann(self, a_y):
        self.assertEqual(a_y.shape, self.target_ann_shape)
        self.assertTrue(np.array_equal(a_y, np.zeros(self.target_ann_shape)))
# TODO: test that sum of annotations stays the same
# TODO: test stacks that are not evenly divisible by step


class TestNoiseAugmenter(TestShiftAugmenter):
    def setUp(self):
        self.load_data()
        self.aug = datahandler.models.NoiseAugmenter()
        self.random_vars = {'scale': 0.1}
        self.result = np.load(os.path.join(testdir, 'noise_junction_extraction_output.npz'),
                              allow_pickle=False)['arr_0']
        self.aug.rng = np.random.default_rng(42)
        self.var_init()
        self.target_shape = self.ms[0].shape


class TestPlotClasses(unittest.TestCase):
    def setUp(self):
        self.ann = datahandler.MultiImageStackClassification()
        self.ann.load(os.path.join(testdir, 'kin1_eval', 'junction_annotations.npz'))
        self.ann_0 = [26116, 20128]
        self.num_cat = [4, 6]
        self.bar_x = [['A1', 'A2', 'B1', 'B2'], ['A1', 'A2', 'B1', 'B2', 'detach', 'land']]
        self.x = [np.arange(4), np.arange(6)]
        self._class = datahandler.evaluate.PlotClasses
        self.pc = self._class()

    def check_attrs(self, check_no=0):
        self.assertIsInstance(self.pc.classification, datahandler.ImageStackClassification)
        self.assertEqual(self.pc.classification.data.shape[0], self.ann_0[check_no])
        self.assertEqual(self.pc.num_cat, self.num_cat[check_no])
        self.assertEqual(self.pc.bar_x, self.bar_x[check_no])
        self.assertTrue(np.array_equal(self.pc.x, self.x[check_no]))

    def test_init(self):
        self.assertIsInstance(self.pc, datahandler.evaluate.PlotClasses)
        self.pc = self._class(isc=self.ann)
        self.check_attrs()

    def test_has_data(self):
        self.assertFalse(self.pc.has_data())
        self.pc = self._class(isc=self.ann)
        self.assertTrue(self.pc.has_data())

    def test_update(self):
        self.pc.update(self.ann)
        self.check_attrs()
        self.pc.update(self.ann + self.ann)
        self.assertEqual(self.pc.classification.data.shape[0], 2 * self.ann_0[0])

    def test_new_data(self):
        corr = datahandler.MultiImageStackClassification()
        corr.load(os.path.join(testdir, 'junction_predicted_manual_corrected.npz'))
        self.pc.update(corr)
        self.check_attrs(1)

    def test_set_x(self):
        self.pc.classification = self.ann


class TestPlotFrames(TestPlotClasses):
    def setUp(self):
        super().setUp()
        self.x = [np.arange(self.ann_0[0]), np.arange(self.ann_0[1])]
        self._class = datahandler.evaluate.PlotFrames
        self.pc = self._class()


class TestPlotSums(TestPlotClasses):
    def setUp(self):
        super().setUp()
        self.ann_0 = [944, 32]
        self.x = [np.arange(self.ann_0[0]), np.arange(self.ann_0[1])]
        self.num_cat = [1, 1]
        self.bar_x = [['0'], ['x:1043 y:1684']]
        self._class = datahandler.evaluate.PlotSums
        self.pc = self._class()


class TestComparePlotClasses(TestPlotClasses):
    def setUp(self):
        super().setUp()
        self.comp = self.ann.copy()
        self.labels = [['predictions', 'corrected'], ['predictions', 'corrected']]
        self._class = datahandler.evaluate.ComparePlotClasses
        self.pc = self._class()

    def check_attrs(self, check_no=0):
        super().check_attrs(check_no)
        self.assertEqual(self.pc.labels, self.labels[check_no])
        self.assertIsInstance(self.pc.compare, datahandler.ImageStackClassification)
        self.assertEqual(self.pc.compare.data.shape[0], self.ann_0[check_no])

    def test_init(self):
        self.assertIsInstance(self.pc, datahandler.evaluate.PlotClasses)
        self.assertFalse(self.pc.has_data())
        self.pc = self._class(isc=self.ann, compare=self.comp)
        self.check_attrs()

    def test_has_data(self):
        self.assertFalse(self.pc.has_data())
        self.pc.update(isc=self.ann, compare=self.comp)
        self.assertTrue(self.pc.has_data())
        comp = datahandler.MultiImageStackClassification()
        comp.load(os.path.join(testdir, 'junction_predicted_mean.npz'))
        self.pc.update(isc=self.ann, compare=comp)
        self.assertFalse(self.pc.has_data())

    def test_update(self):
        self.pc.update(isc=self.ann, compare=self.comp)
        self.check_attrs()
        self.pc.update(isc=self.ann + self.ann, compare=self.comp + self.comp)
        self.assertEqual(self.pc.classification.data.shape[0], 2 * self.ann_0[0])
        self.assertEqual(self.pc.compare.data.shape[0], 2 * self.ann_0[0])

    def test_new_data(self):
        corr = datahandler.MultiImageStackClassification()
        corr.load(os.path.join(testdir, 'junction_predicted_manual_corrected.npz'))
        comp = datahandler.MultiImageStackClassification()
        comp.load(os.path.join(testdir, 'junction_predicted_mean.npz'))
        self.pc.update(isc=corr, compare=comp)
        self.check_attrs(1)

    def test_set_x(self):
        self.pc.update(isc=self.ann, compare=self.comp)


class TestComparePlotFrames(TestComparePlotClasses):
    def setUp(self):
        super().setUp()
        self.x = [np.arange(self.ann_0[0]), np.arange(self.ann_0[1])]
        self._class = datahandler.evaluate.ComparePlotFrames
        self.pc = self._class()


class TestComparePlotSums(TestComparePlotClasses):
    def setUp(self):
        super().setUp()
        self.ann_0 = [944, 32]
        self.x = [np.arange(self.ann_0[0]), np.arange(self.ann_0[1])]
        self.num_cat = [1, 1]
        self.bar_x = [['0'], ['x:1043 y:1684']]
        self._class = datahandler.evaluate.ComparePlotSums
        self.pc = self._class()


if __name__ == '__main__':
    unittest.main()
