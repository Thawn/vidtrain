import numpy as np
import matplotlib
from matplotlib import pyplot as plt
from . import interface
from . import data


class PlotClasses(interface.EvalPlot):
    savename = 'plot_classes.svg'

    def __init__(self, isc=None, fig=None):
        self.fig = fig or plt.figure()
        self.ax = self.fig.add_subplot(1, 1, 1)
        self.num_cat = 0
        self.bar_x = []
        self.x = []
        try:
            self.classification = isc
        except TypeError:
            pass

    @property
    def classification(self):
        return self._isc

    @classification.setter
    def classification(self, misc):
        if isinstance(misc, interface.MultiImageStackClassification):
            self._isc = self._convert_data(misc)
        else:
            self._isc = misc
        if self.has_data():
            self._update()

    def _convert_data(self, misc: interface.MultiImageStackClassification):
        return data.ImageStackClassification(
            data=np.concatenate(misc),
            category_names=list(misc.category_names))

    def _update(self):
        self.num_cat = self._isc.data.shape[1]
        self.bar_x = list(self._isc.category_names)[:self.num_cat]
        self.ax.clear()
        self.set_x()
        self.plot()
        self.format_plot()
        self.fig.tight_layout()

    def update(self, isc: interface.ImageStackClassification):
        self.classification = isc

    def set_x(self):
        self.x = np.arange(len(self.bar_x))

    def plot(self):
        y = np.sum(self.classification.data, axis=0)
        self.ax.bar(self.x, y, yerr=np.sqrt(y))

    def format_plot(self):
        self.ax.set_ylabel('events')
        self.ax.yaxis.set_major_locator(matplotlib.ticker.MaxNLocator(integer=True, min_n_ticks=1))
        self.ax.tick_params(axis='x', labelrotation=self._calc_rotation())
        self.ax.tick_params(direction='in')
        self.ax.set_xticks(self.x)
        self.ax.set_xticklabels(self.bar_x)

    def has_data(self):
        try:
            return isinstance(self.classification, interface.ImageStackClassification)
        except AttributeError:
            return False

    def _calc_rotation(self):
        return (max(map(len, self.bar_x)) > 4) * 90


class PlotFrames(PlotClasses):
    savename = 'plot_frames.svg'

    def set_x(self):
        self.x = np.arange(self.classification.data.shape[0])

    def plot(self):
        for cat in range(self.num_cat):
            self.ax.plot(self.x, self.classification.data[:, cat], label=self.bar_x[cat])

    def format_plot(self):
        self.ax.set_ylabel('probability')
        self.ax.set_xlabel('frame')
        self.ax.legend()
        self.common_formatting(self.ax)

    def common_formatting(self, axes):
        axes.yaxis.set_major_formatter(matplotlib.ticker.FormatStrFormatter('%.1f'))
        axes.xaxis.set_major_locator(matplotlib.ticker.MaxNLocator(integer=True, prune='both'))
        axes.tick_params(direction='in')
        axes.set_ylim(0, 1)


class PlotSums(PlotClasses):
    savename = 'plot_sums.svg'

    def _convert_data(self, misc: interface.MultiImageStackClassification):
        if not isinstance(misc, interface.MultiImageStackClassification):
            raise TypeError(
                'expecting a MultiImageStackClassification object. Got {} instead.'.format(type(misc)))
        junction_data = misc.np_array()
        if len(junction_data.shape) > 1:
            junction_data = np.sum(junction_data, axis=tuple(range(1, junction_data.ndim)))[..., np.newaxis]
        else:
            js = np.zeros(junction_data.shape)
            for i, j in enumerate(junction_data):
                js[i] = np.sum(j)
            junction_data = js[..., np.newaxis]
        return data.ImageStackClassification(data=junction_data, category_names=misc.names)

    def set_x(self):
        self.x = np.arange(self.classification.data.shape[0])

    def plot(self):
        y = self.classification.data[:, 0]
        self.ax.bar(self.x, y, yerr=np.sqrt(y))

    def format_plot(self):
        self.ax.tick_params(direction='in')
        self.ax.set_xlabel('junctions')
        self.ax.set_ylabel('events')
        self.ax.yaxis.set_major_locator(matplotlib.ticker.MaxNLocator(integer=True, prune='both'))


class ComparePlotClasses(PlotClasses):
    def __init__(self, isc=None, compare=None, labels=['predictions', 'corrected'], fig=None):
        PlotClasses.__init__(self, isc=isc, fig=fig)
        self.labels = labels
        try:
            self.compare = compare
        except TypeError:
            pass

    @property
    def compare(self):
        return self._comp

    @compare.setter
    def compare(self, misc):
        if isinstance(misc, interface.MultiImageStackClassification):
            self._comp = self._convert_data(misc)
        else:
            self._comp = misc
        if self.has_data():
            self._update()

    def update(self, isc: interface.MultiImageStackClassification, compare: interface.MultiImageStackClassification):
        self.classification = isc
        self.compare = compare

    def plot(self):
        width = 0.35
        y_d = np.sum(self.classification.data, axis=0)
        y_c = np.sum(self.compare.data, axis=0)
        self.ax.bar(self.x - width / 2, y_d, width, yerr=np.sqrt(y_d), label=self.labels[0])
        self.ax.bar(self.x + width / 2, y_c, width, yerr=np.sqrt(y_c), label=self.labels[1])

    def format_plot(self):
        super().format_plot()
        self.ax.legend()

    def has_data(self):
        try:
            return isinstance(self.classification, interface.ImageStackClassification) and isinstance(self.compare, interface.ImageStackClassification) and self.classification.data.shape[0] == self.compare.data.shape[0]
        except AttributeError:
            return False


class ComparePlotFrames(ComparePlotClasses, PlotFrames):
    def __init__(self, isc=None, compare=None, labels=['predictions', 'corrected'], fig=None):
        # need to set compare later otherwise _update fails because of missing ax2
        ComparePlotClasses.__init__(self, isc=isc, compare=None, labels=labels, fig=fig)
        self.ax.remove()
        self.ax2 = self.fig.add_subplot(2, 1, 1)
        self.ax = self.fig.add_subplot(2, 1, 2)
        try:
            self.compare = compare
        except TypeError:
            pass

    def update(self, isc: interface.ImageStackClassification, compare: interface.ImageStackClassification):
        self.ax2.clear()
        self.compare = compare
        self.classification = isc
        self.format_top_plot()
        self.fig.tight_layout()

    def plot(self):
        frames = np.arange(self.classification.data.shape[0])
        for cat in range(self.num_cat):
            self.ax2.plot(frames, self.classification.data[:, cat], label=self.bar_x[cat])
            self.ax.plot(frames, self.compare.data[:, cat], label=self.bar_x[cat])

    def format_top_plot(self):
        self.ax2.set_ylabel(self.labels[0] + ' prob.')
        self.ax.set_ylabel(self.labels[1] + ' prob.')
        self.ax.legend(fontsize=9)
        self.ax2.tick_params(labelbottom=False)
        self.common_formatting(self.ax2)


class ComparePlotSums(ComparePlotClasses, PlotSums):
    def set_x(self):
        PlotSums.set_x(self)

    def plot(self):
        width = 0.35
        y_d = self.classification.data[:, 0]
        y_c = self.compare.data[:, 0]
        self.ax.bar(self.x - width / 2, y_d, width, yerr=np.sqrt(y_d), label=self.labels[0])
        self.ax.bar(self.x + width / 2, y_c, width, yerr=np.sqrt(y_c), label=self.labels[1])
